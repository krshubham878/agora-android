package org.aossie.agoraandroid.data.network.responses

data class Token(
  var token: String?,
  var expiresOn: String?
)